'''
Created on 2 oct. 2020

@author: RSSpe
'''

from Aspirante import Aspirante


class RegistroAspirantes():
    
    def __init__(self):
        self.listaAspirantes = []


    def buscarAspirante(self):
        if not len(self.listaAspirantes)==0:
            cont = 0
            
            print()
            while(True):
                try:
                    folio = int(input("Introduce folio a buscar: "))

                    if folio>0:
                        break
                    else:
                        print("Folio invalido, por favor vuelve a intentarlo\n")
                except ValueError as error:
                    print("Folio invalido<", error,">, por favor vuelve a intentarlo")
            
            for i in self.listaAspirantes:
                if folio==i.folio:
                    print(i)
                else:
                    cont+=1
            
            if(cont==len(self.listaAspirantes)):
                print("No existe ese aspirante") 

        else:
            print("No hay elementos")
        print()


    def mostrarAspirantes(self):
        if not len(self.listaAspirantes)==0:
            print()
            for i in self.listaAspirantes:
                print(i)

        else:
            print("No hay elementos")
        print()


    def registroAspirante(self):
        folio = 0
        redes = ""

        print()
        nombre = input("Introduce nombre: ")

        while(True):
            try:
                edad = int(input("Introduce edad: "))
            
                if edad>0:
                    break
                else:
                    print("No puedes ingresar esa edad, por favor vuelve a intentarlo")
            except ValueError as error:
                print("Valor incorrecto <", error,">, por favor vuelve a intentarlo\n")


        redes = input("Introduce redes sociales. ej:(Facebook, Twitter, Instagram): ")
        redes = redes.replace(" ", "")
        
        if len(self.listaAspirantes)==0:
            folio = 1
        else:
            folio = self.listaAspirantes[len(self.listaAspirantes)-1].folio+1

        self.listaAspirantes.append(Aspirante(folio, nombre, edad, redes))
        print("Aspirante registrado\n")


    def eliminarAspirante(self):
        if not len(self.listaAspirantes)==0:
            cont = 0
            
            print()
            while(True):
                try:
                    folio = int(input("Introduce folio a eliminar: "))

                    if folio>0:
                        break
                    else:
                        print("Folio invalido, por favor vuelve a intentarlo\n")
                except ValueError as error:
                    print("Folio invalido<", error,">, por favor vuelve a intentarlo")
        
            for i in self.listaAspirantes:
                if folio==i.folio:
                    print("Se elimino a un aspirante")
                    self.listaAspirantes.remove(i)
                    return i
                else:
                    cont+=1

            if(cont==len(self.listaAspirantes)):
                print("No existe ese aspirante")
                return -1

        else:
            print("No hay elementos")
            return 0
        print()

