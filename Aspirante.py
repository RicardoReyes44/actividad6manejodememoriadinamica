'''
Created on 2 oct. 2020

@author: RSSpe
'''

class Aspirante:
    
    def __init__(self, folio, nombre, edad, redesSociales):
        self.__folio = folio
        self.__nombre = nombre
        self.__edad = edad
        self.__redesSociales = redesSociales

    @property
    def folio(self):
        return self.__folio
    
    @property
    def nombre(self):
        return self.__nombre
    
    @property
    def edad(self):
        return self.__edad

    @property
    def redesSociales(self):
        return self.__redesSociales

    def __str__(self):
        return f"folio: {self.__folio}, nombre: {self.nombre}, edad: {self.edad}, redesSociales: {self.redesSociales}"