'''
Created on 2 oct. 2020

@author: RSSpe
'''

from RegistroAspirantes import RegistroAspirantes

def main():
    
    candado = True
    ra = RegistroAspirantes()

    while(candado):
        print("---------------------\nRegistro de aspirantes\n---------------------");
        print("1.- Registrar aspirante");
        print("2.- Mostrar aspirantes");
        print("3.- Buscar aspirante");
        print("4.- Eliminar aspirantes");
        print("5.- Salir");
        
        try:
            opcion = int(input("Introduce opcion: "))
        
            if opcion==1:
                ra.registroAspirante()

            elif opcion==2:
                ra.mostrarAspirantes()

            elif opcion==3:
                ra.buscarAspirante()

            elif opcion==4:
                print(f"{ra.eliminarAspirante()}")

            elif opcion==5:
                candado = False
        
            else:
                print("Opcion inexistente, por favor prueba de nuevo")
        except ValueError as error:
            print("Error en la entrada<", error, ">, por favor vuelve a intentarlo")
    
    print("Programa terminado")


if __name__ == '__main__':
    main()